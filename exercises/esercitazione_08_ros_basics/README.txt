1. Creare un package_ros, di nome simple_sim avente come dipendenze
   roscpp

   suggerimento:
    usare il comando catkin_create_pkd dalla directory ce conterra' il package
    
2. all'interno del package, definire i seguenti messaggi
   Pose: float32 x, float32 y, float32 theta
   Cmd:  float32 linear_speed, float32 angular_speed

   Editare CMakeFile ed assicurarsi della  corretta compilazione dei messaggi.
   Devono essere visibili mediante rosmsg show

3. aggiungere al nodo il servizio
   SetTimeInterval:
     request: float32 period
     response: float32 period

   Editare il makefile ed assicurarsi della corretta compilazione dei messaggi

4. aggiungere un nodo SimpleSimNode
   - il nodo pubblica periodicamente (con periodo T, memorizzato in una variabile
   globale), un messaggio di tipo "Pose" su un topic /simplesim/pose.
   I valori di pose sono settati a zero.

   Compilare e lanciare il nodo.
   Verificare che il nodo pubblichi l'informazione.

5. Estendere il nodo SimpleSimNode, aggiungendo la sottoscrizione
   ad im messaggio di tipo Cmd dal topic /simplesim/cmd.
   alla ricezione del messaggio,
   il nodo stampa su console i valori del messaggio, e li setta in variabili globali
   (linear_speed e angular_speed)
   
   Testarlo, inviando manualmente dei messaggi con rostopic pub

6. Estendere il nodo aggiungendo un servizio
   /simplesim/set_interval, che quando riceve una richiesta,
   - se il valore di period e' <0, il nodo risponde con il valore di T corrente
   - se il valore di period e' >0, il nodo assegna a T il valore di period,
     e lo restituisce.

   Verificare il funzionamento del servizio, inviando richieste
   mentre monitorate i messaggi simplesim/pose. La loro frequenza deve cambiare

7. Estendere il nodo in modo tale che ad ogni periodo aggiorni la posizione
   del robot, in base alle velocita' lineare ed angolare del veicolo.


   Il processo di integrazione e' il seguente
   // calcolo spostmento in robot frame
   - se omega=0
     dx=linear_speed;
     dy=0;
     dtheta=0;
   - altrimenti
     R =linear_speed/angular_speed; //(raggio curvatura)
     dtheta= angular_speed*T;         //(arco lungo il raggio)
     dx     =R*sin(dtheta);
     dy     =R*(1-cos(dtheta))

   //posa successiva
   x += cos(theta) * dx - sin(theta) * dy;
   y + =sin(theta  * dx + cos(theta) * dy;
   theta += dtheta;
