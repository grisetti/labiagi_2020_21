#include <fstream>
#include <iostream>
#include <Eigen/StdVector>
#include "eigen_01_point_loading.h"
#include <Eigen/Core>
#include <Eigen/Geometry>

using namespace std;
using ContainerType = Vector2fVector;

int main(int argc, char** argv) {
  if (argc<3) {
    cerr << " usage: "
         << argv[0]
         << " <output_file> <num_points> <scale>"<< endl;
    return 0;
  }
  float scale=1;
  int num_points=atoi(argv[2]);

  if (argc>3) {
    scale=atof(argv[3]);
  }

  cerr << "generating " << num_points << " random 2D points at scale " << scale << endl;

  ContainerType output;
  for (int i=0; i<num_points; ++i) {
    Eigen::Vector2f v=Eigen::Vector2f::Random()*scale;
    output.push_back(v);
  }
  
  ofstream os(argv[1]);
  if (! os.good()) {
    cerr << "can't open file [" << argv[1] << "]" << endl;
    return 0;
  }
  savePoints(os, output);
  return 0;
}
