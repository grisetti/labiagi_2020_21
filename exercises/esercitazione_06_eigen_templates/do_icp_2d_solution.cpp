#include <fstream>
#include <iostream>
#include "utils_2d_solutions.h"
#include "eigen_01_point_loading.h"
#include "eigen_03_covariance.h"
#include "eigen_04_kdtree.h"

using namespace std;
using ContainerType = Vector2fVector;
using KDTreeType    = TreeNodeBase_<Vector2fVector>;
  

inline float errorAndJacobian(Eigen::Vector2f& error,
                       Eigen::Matrix<float, 2,3>& jacobian,
                       const Eigen::Vector2f& fixed,
                       const Eigen::Vector2f& moving) {
  error=moving-fixed;
  jacobian(0,2) = -moving.y();
  jacobian(1,2) =  moving.x();
  return error.squaredNorm();
}



Eigen::Isometry2f doICP2d (const ContainerType& fixed,
                           const ContainerType& moving,
                           const Eigen::Isometry2f& guess) {

  // fixed parameters
  int num_iterations = 5;
  int points_per_leaf = 10;
  int min_num_points  = 4;
  float max_distance = 50;
  float saturation_chi = 25;
  
  //1. make a copy of fixed
  auto temp_fixed = fixed;

  //2. construct a kd tree out of temp_fixed;
  auto kd_tree=KDTreeType::buildTree(temp_fixed.begin(), temp_fixed.end(), points_per_leaf);


  //4. our estimate is the same as the initial guess
  Eigen::Isometry2f estimate = guess;
  
  Eigen::Matrix3f H;
  Eigen::Vector3f b;
  for (int i=0; i<num_iterations; ++i) {

    // BEGIN for solver 
    H.setZero();
    b.setZero();
    Eigen::Vector2f error;
    Eigen::Matrix<float, 2,3> jacobian;
    float chi2_total=0;
    int num_points=0;
    jacobian.block<2,2>(0,0).setIdentity();
    // END for solver 

    std::string fname("do_icp_");
    fname += std::to_string(i) + ".dat";
    ofstream os(fname);

    for (const auto& m: moving) {
      auto tm=estimate*m; // in tm the point transformed
      Vector2f best_match;
      //3. allocate a vector of candidate points (should be allocated once at the beginning)
      ContainerType neighbors;
      kd_tree->search(neighbors, tm, max_distance);
      if (neighbors.empty())
        continue;
      
      best_match=bestMatch(neighbors, tm);

      os <<  best_match.transpose() << endl << tm.transpose() << endl<< endl;

      // BEGIN for solver
      float chi = errorAndJacobian(error, jacobian, best_match, tm);
      float scale = (chi < saturation_chi) ? 1.f : sqrt(saturation_chi/chi);
      H.noalias() += scale * jacobian.transpose()*jacobian;
      b.noalias() += scale * jacobian.transpose()*error;
      ++ num_points;
      chi2_total += chi; 
      // END for solver
   }

    if (num_points<min_num_points)
      return estimate;

    Vector3f dx=H.ldlt().solve(-b);
    estimate=v2t(dx)*estimate;
    cerr << "iteration: " << i
         << ", num_points: " << num_points
         << ", chi2_total: " << chi2_total
         << ", chi2_inlier: " << chi2_total/num_points 
         << ", dx: " << dx.transpose() << endl;
  }
  return estimate;
}

int main(int argc, char** argv) {
  ContainerType fixed;
  ContainerType moving;

  if (argc<6) {
    cerr << " usage: "
         << argv[0]
         << " <fixed_file> <moving_file> <x> <y> <theta> "<< endl;
    return 0;
  }
  ifstream is_fixed(argv[1]);
  if (! is_fixed.good()) {
    cerr << "can't open file [" << argv[1] << "]" << endl;
    return 0;
  }
  loadPoints(fixed, is_fixed);
  
  ifstream is_moving(argv[2]);
  if (! is_moving.good()) {
    cerr << "can't open file [" << argv[2] << "]" << endl;
    return 0;
  }
  loadPoints(moving, is_moving);

  Vector3f guess;
  guess << atof(argv[3]), atof(argv[4]), atof(argv[5]);

  cerr << "Guess: " << guess.transpose()  << endl;
  
  Eigen::Isometry2f estimate = doICP2d(fixed, moving, v2t(guess));
  cerr << "Estimate: " << t2v(estimate).transpose() << endl;
  return 0;
}
