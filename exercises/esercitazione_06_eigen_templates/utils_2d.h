#pragma once
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <vector>
#include <Eigen/Geometry>
#include <Eigen/Cholesky>
#include "eigen_01_point_loading.h"
#include "eigen_03_covariance.h"
#include "eigen_04_kdtree.h"
#include <limits>

using ContainerType = Vector2fVector;

// write a function that constructs a 2d isometry out of a 3 vector (x,y, theta)
Eigen::Isometry2f v2t(const Vector3f& dx) {
  Eigen::Isometry2f dX;
 
  dX.setIdentity();
  // fill me
  //...
  return dX;
}

// write a function that constructs a 3 vector (x,y, theta) out of a 2d isometry
Eigen::Vector3f t2v(const Eigen::Isometry2f& dX) {
  return Eigen::Vector3f::Zero();
  // fill me
  //...
}


// write a function that scans the linear container and returns the point
// in the container closest to p
Eigen::Vector2f bestMatch(const ContainerType& cont, const Vector2f& p) {
  return Vector2f::Zero();
  // fill me
  //...
}
