#include "utils_2d.h"
#include <fstream>
#include <iostream>

using namespace std;
using ContainerType = Vector2fVector;
using KDTreeType    = TreeNodeBase_<Vector2fVector>;

int main(int argc, char** argv) {
  ContainerType points;

  if (argc<6) {
    cerr << " usage: "
         << argv[0]
         << " <input> <output> <x> <y> <theta> "<< endl;
    return 0;
  }
  ifstream is(argv[1]);
  if (! is.good()) {
    cerr << "can't open file [" << argv[1] << "]" << endl;
    return 0;
  }
  loadPoints(points, is);


  Vector3f x;
  x << atof(argv[3]), atof(argv[4]), atof(argv[5]);
  Eigen::Isometry2f bigX=v2t(x);

  for (auto& p: points) {
    p=(bigX*p);
  }

  ofstream os(argv[2]);
  if (! os.good()) {
    cerr << "can't open file [" << argv[2] << "]" << endl;
    return 0;
  }
  savePoints(os, points);
  return 0;
}
