#include "utils_2d.h"
#include "eigen_01_point_loading.h"
#include "eigen_03_covariance.h"
#include "eigen_04_kdtree.h"
#include <fstream>
#include <iostream>

using namespace std;
using ContainerType = Vector2fVector;
using KDTreeType    = TreeNodeBase_<Vector2fVector>;
  

void findMatches (const ContainerType& fixed,
                  const ContainerType& moving,
                  const std::string fname) {

  // fixed parameters
  int points_per_leaf = 10;
  float max_distance = 50; // maximum squared distance for a match
  
  //1. make a copy of fixed
  auto temp_fixed = fixed;

  //2. construct a kd tree out of temp_fixed;
  auto kd_tree=KDTreeType::buildTree(temp_fixed.begin(), temp_fixed.end(), points_per_leaf);

  ofstream os(fname); // we open the stream...

  for (const auto& m: moving) {
    Vector2f best_match;
    ContainerType neighbors;
    /// FILL ME, use the kd tree and look in the neighbots
    
    os <<  best_match.transpose() << endl << m.transpose() << endl<< endl;
  }
}

int main(int argc, char** argv) {
  ContainerType fixed;
  ContainerType moving;

  if (argc<3) {
    cerr << " usage: "
         << argv[0]
         << " <fixed_file> <moving_file> <output_file>"<< endl;
    return 0;
  }
  ifstream is_fixed(argv[1]);
  if (! is_fixed.good()) {
    cerr << "can't open file [" << argv[1] << "]" << endl;
    return 0;
  }
  loadPoints(fixed, is_fixed);
  
  ifstream is_moving(argv[2]);
  if (! is_moving.good()) {
    cerr << "can't open file [" << argv[2] << "]" << endl;
    return 0;
  }
  loadPoints(moving, is_moving);
  std::string file_prefix = "matches.dat";
  if (argc>3)
    file_prefix=argv[3];
  cerr << "writing output to [" << file_prefix << "]" << endl;
  findMatches(fixed, moving, file_prefix);
  return 0;
}
