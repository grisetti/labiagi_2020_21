#pragma once
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <vector>
#include <Eigen/Geometry>
#include <Eigen/Cholesky>
#include "eigen_01_point_loading.h"
#include "eigen_03_covariance.h"
#include "eigen_04_kdtree.h"
#include <limits>

using ContainerType = Vector2fVector;

// write a function that constructs a 2d isometry out of a 3 vector (x,y, theta)
Eigen::Isometry2f v2t(const Vector3f& dx) {
  Eigen::Isometry2f dX;
  dX.setIdentity();
  float c=cos(dx(2));
  float s=sin(dx(2));
  dX.linear() <<
    c, -s,
    s,  c,
    dX.translation() << dx(0), dx(1);
  return dX;
}

// write a function that constructs a 3 vector (x,y, theta) out of a 2d isometry
Eigen::Vector3f t2v(const Eigen::Isometry2f& dX) {
  const float& c=dX.linear()(0,0);
  const float& s=dX.linear()(1,0);
  return Eigen::Vector3f(dX.translation().x(),
                         dX.translation().y(),
                         atan2(s,c));
}


// write a function that scans the linear container and returns the point
// in the container closest to p
Vector2f bestMatch(const ContainerType& cont, const Vector2f& p) {
  Vector2f best_match;
  float d_min=std::numeric_limits<float>::max();
  for (const auto& c:cont) {
    float d=(c-p).squaredNorm();
    if (d<d_min) {
      d_min=d;
      best_match=c;
    }
  }
  return best_match;
}
