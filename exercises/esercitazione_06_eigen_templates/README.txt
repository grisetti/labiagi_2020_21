1. in utils.h:
   - implement the functions v2t and t2v to turn an eigen isometry(2d)
     into a 3d vector and viceversa

   - implement the function bestMatch(), that takes a container of 2d points
     and a point and returns the query point in the container that
     is closest to the query

2. implement a transform_points program that takes a set of points (a file),
   a transform, and applies the transform to all points, and writes them
   in another file

3. complete the find_matches program, that given two sets of points,
   for each point in the second set
       finds the closest point in the first set.
   Use a KD-Tree

4. at the end try to compile the doICP program, and repeat the results
   we got at lesson

