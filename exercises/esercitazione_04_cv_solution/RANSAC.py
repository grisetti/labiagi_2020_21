from __future__ import print_function
import cv2 as cv
import numpy as np
import argparse
from time import time
from math import sqrt
import random

''' 
Random Sample Consensus (RANSAC) is one of the robust probabilistic methods used for removing 
the outliers from matched features and fitting the transformation function (in terms of homography 
matrix). This matrix provides the perspective transform of second image with respect to the first 
(reference image). Image reconstruction is then performed on the basis of the derived transformation 
function to align the second image with respect to the first.
'''


HIGHER_INLIERS = 0


def init(args, nn_matches):
    global HIGHER_INLIERS 
    global BEST_HOMOGRAPHY 
    global RESULT_IMAGE
    global GOOD_MATCHES

    ## [ratio test filtering]
    matched1 = []
    matched2 = []
    good = []
    nn_match_ratio = 0.8 # Nearest neighbor matching ratio
    for m, n in nn_matches:
        if m.distance < nn_match_ratio * n.distance:
            matched1.append(kpts1[m.queryIdx])
            matched2.append(kpts2[m.trainIdx])
            good.append(m)
            #print(good)
    ## [ratio test filtering]

    # choosing 4 random points
    points = []
    for i in range(4):
        p = random.choice(good)
        points.append(p)


    ## extract the matched keypoints
    src_pts = np.float32([ kpts1[m.queryIdx].pt for m in points ]).reshape(-1,1,2)
    dst_pts = np.float32([ kpts2[m.trainIdx].pt for m in points ]).reshape(-1,1,2)

    ## find homography matrix and do perspective transform
    H = cv.getPerspectiveTransform(src_pts, dst_pts) 
    print('Homography\n')
    print(H)

    ## [homography check]
    inliers1 = []
    inliers2 = []
    good_matches = []
    inlier_threshold = 2.5 # Distance threshold to identify inliers with homography check
    for i, m in enumerate(matched1):
        col = np.ones((3,1), dtype=np.float64)
        col[0:2,0] = m.pt

        col = np.dot(H, col)
        col /= col[2,0]
        dist = sqrt(pow(col[0,0] - matched2[i].pt[0], 2) +\
                    pow(col[1,0] - matched2[i].pt[1], 2))

        if dist < inlier_threshold:
            good_matches.append(cv.DMatch(len(inliers1), len(inliers2), 0))
            inliers1.append(matched1[i])
            inliers2.append(matched2[i])
            #print(good_matches)
            

    source = np.float32([ kpts1[m.queryIdx].pt for m in good_matches ]).reshape(-1,1,2)
    destin = np.float32([ kpts2[m.queryIdx].pt for m in good_matches ]).reshape(-1,1,2)
    ## [homography check]

    ## [draw final matches]
    res = np.empty((max(img1.shape[0], img2.shape[0]), img1.shape[1]+img2.shape[1], 3), dtype=np.uint8)
    cv.drawMatches(img1, inliers1, img2, inliers2, good_matches, res)
    #cv.imwrite("matching_result.png", res)

    inlier_ratio = len(inliers1) / float(len(matched1))
    print(f'{args.feature} Matching Results')
    print('*******************************')
    print('# Keypoints 1:                        \t', len(kpts1))
    print('# Keypoints 2:                        \t', len(kpts2))
    print('# Matches:                            \t', len(matched1))
    print('# Inliers:                            \t', len(inliers1))
    print('# Inliers Ratio:                      \t', inlier_ratio)

    if len(inliers1) > HIGHER_INLIERS:
        HIGHER_INLIERS = len(inliers1)
        BEST_HOMOGRAPHY = H
        RESULT_IMAGE = res
        GOOD_MATCHES = good_matches
        print('---------------------------')
        print('\nHIGHER INLIERS/n')
        print(HIGHER_INLIERS)
        print('\nBEST HOMOGRAPHY\n')
        print(BEST_HOMOGRAPHY)
        print('---------------------------')
        
    return HIGHER_INLIERS, BEST_HOMOGRAPHY, RESULT_IMAGE, GOOD_MATCHES
    #cv.imshow('result', res)
    #cv.waitKey()
    ## [draw final matches]


if __name__ == "__main__":
    ## [load]
    parser = argparse.ArgumentParser(description='Code for AKAZE local features matching tutorial.')
    parser.add_argument('--input1', help='Path to input image 1.', default='./matching/graf1.png')
    parser.add_argument('--input2', help='Path to input image 2.', default='./matching/graf3.png')
    #parser.add_argument('--homography', help='Path to the homography matrix.', default='data/matching/H1to3p.xml')
    parser.add_argument('--feature', help='Feature extractor type.', default='SIFT')
    args = parser.parse_args()

    img1 = cv.imread(cv.samples.findFile(args.input1), cv.IMREAD_GRAYSCALE)
    img2 = cv.imread(cv.samples.findFile(args.input2), cv.IMREAD_GRAYSCALE)
    if img1 is None or img2 is None:
        print('Could not open or find the images!')
        exit(0)

    #fs = cv.FileStorage(cv.samples.findFile(args.homography), cv.FILE_STORAGE_READ)
    #homography = fs.getFirstTopLevelNode().mat()
    ## [load]

    if args.feature == 'SIFT':
        start = time()
        extractor = cv.SIFT_create()
        kpts1, desc1 = extractor.detectAndCompute(img1, None)
        kpts2, desc2 = extractor.detectAndCompute(img2, None)
        end = time()

        matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_L1)
        nn_matches = matcher.knnMatch(desc1, desc2, 2)
    else:
        args.feature = "A-KAZE"
        start = time()
        extractor = cv.AKAZE_create()
        kpts1, desc1 = extractor.detectAndCompute(img1, None)
        kpts2, desc2 = extractor.detectAndCompute(img2, None)
        end = time()

        matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_HAMMING)
        nn_matches = matcher.knnMatch(desc1, desc2, 2)

    print(f"Extraction time: {end-start}")

    for i in range(100):
        inliers, homography, res, good_matches = init(args, nn_matches)
    print('\n\n')
    print('----- FINAL RESULTS -----')
    print('\nInliers\n')
    print(inliers)
    print('\nHomography\n')
    print(homography)


    ## extract the matched keypoints
    src_pts = np.float32([ kpts1[m.queryIdx].pt for m in good_matches ]).reshape(-1,1,2)
    dst_pts = np.float32([ kpts2[m.trainIdx].pt for m in good_matches ]).reshape(-1,1,2)

    ## find homography matrix considering all inliers
    M, _ = cv.findHomography(src_pts, dst_pts, 0)
    print('\nHomography\n')
    print(M)

    cv.imshow('Result', res)
    cv.imwrite("matching_result.png", res)
    cv.waitKey()