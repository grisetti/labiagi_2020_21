from __future__ import print_function
import cv2 as cv
import numpy as np
import argparse
from time import time
from math import sqrt

## [load]
parser = argparse.ArgumentParser(description='Code for AKAZE local features matching tutorial.')
parser.add_argument('--input1', help='Path to input image 1.', default='./matching/graf1.png')
parser.add_argument('--input2', help='Path to input image 2.', default='./matching/graf3.png')
parser.add_argument('--homography', help='Path to the homography matrix.', default='./matching/H1to3p.xml')
parser.add_argument('--feature', help='Feature extractor type.', default='SIFT')
args = parser.parse_args()

img1 = cv.imread(cv.samples.findFile(args.input1), cv.IMREAD_GRAYSCALE)
img2 = cv.imread(cv.samples.findFile(args.input2), cv.IMREAD_GRAYSCALE)
if img1 is None or img2 is None:
    print('Could not open or find the images!')
    exit(0)

fs = cv.FileStorage(cv.samples.findFile(args.homography), cv.FILE_STORAGE_READ)
homography = fs.getFirstTopLevelNode().mat()
## [load]

# SIFT is robustly invariant to image rotations, scale, and limited affine variations but its 
# main drawback is high computational cost (patented with SURF in some opencv-python versions)

if args.feature == 'SIFT':
    start = time()
    extractor = cv.SIFT_create()
    kpts1, desc1 = extractor.detectAndCompute(img1, None)
    kpts2, desc2 = extractor.detectAndCompute(img2, None)
    end = time()

    matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_L1)
    nn_matches = matcher.knnMatch(desc1, desc2, 2)
    window = 'SIFT result'

# ORB features are invariant to scale, rotation and limited affine changes
# ORB uses BRIEF descriptors
elif args.feature == 'ORB': 

    start = time()
    extractor = cv.ORB_create()
    kpts1, desc1 = extractor.detectAndCompute(img1, None)
    kpts2, desc2 = extractor.detectAndCompute(img2, None)
    end = time()

    matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_HAMMING)
    nn_matches = matcher.knnMatch(desc1, desc2, 2)
    window = 'ORB result'

# BRIEF performs poorly with rotation
elif args.feature == 'BRIEF':
    start = time()
    # Initiate FAST detector
    star = cv.xfeatures2d.StarDetector_create()
    extractor = cv.xfeatures2d.BriefDescriptorExtractor_create()

    # find the keypoints with STAR in image 1
    kpts1 = star.detect(img1, None)
    # compute the descriptors with BRIEF in image 1
    kpts1, desc1 = extractor.compute(img1, kpts1)

    # find the keypoints with STAR im image 2
    kpts2 = star.detect(img2, None)
    # compute the descriptors with BRIEF in image 2
    kpts2, desc2 = extractor.compute(img2, kpts2)
    end = time()

    matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_L1)
    nn_matches = matcher.knnMatch(desc1, desc2, 2)
    window = 'BRIEF result'

elif args.feature == "MY_DESCRIPTOR": # Our descriptor 

    # Padding
    gray1_ex = cv.copyMakeBorder(img1, 15, 15, 15, 15, cv.BORDER_REFLECT)
    gray2_ex = cv.copyMakeBorder(img2, 15, 15, 15, 15, cv.BORDER_REFLECT)
    
    start = time()

    # Shi-Tomasi corner detector
    corners1 = cv.goodFeaturesToTrack(img1, 60, 0.01, 5)
    corners1 = np.int0(corners1)
 
    corners2 = cv.goodFeaturesToTrack(img2, 60, 0.01, 5)
    corners2 = np.int0(corners2)

    # Keypoints for the extended images
    kpts1 = cv.KeyPoint.convert(corners1)
    kpts2 = cv.KeyPoint.convert(corners2)

    # Descriptors
    descriptors1 = []
    for i in corners1+(15,15):
        y, x = i.ravel()
        descr = gray1_ex[x-15:x+15,y-15:y+15].flatten()
        descriptors1.append(descr)
        
    descriptors2 = []
    for i in corners2+(15,15):
        y, x = i.ravel()
        descr = gray2_ex[x-15:x+15,y-15:y+15].flatten()
        descriptors2.append(descr)

    desc1 = np.array(descriptors1)
    desc2 = np.array(descriptors2)

    end = time()

    matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_L1)
    nn_matches = matcher.knnMatch(desc1, desc2, 2)
    window = 'MY DESCRIPTOR result'


# AKAZE features are invariant to scale, rotation, limited affine and have more distinctiveness at 
# varying scales because of nonlinear scale spaces

else:  
    args.feature = "A-KAZE" 
    start = time()
    extractor = cv.AKAZE_create()
    kpts1, desc1 = extractor.detectAndCompute(img1, None)
    kpts2, desc2 = extractor.detectAndCompute(img2, None)
    end = time()

    matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_HAMMING)
    nn_matches = matcher.knnMatch(desc1, desc2, 2)
    window = 'A-KAZE result'

print(f"Extraction time: {end-start}")


## [ratio test filtering]
matched1 = []
matched2 = []
nn_match_ratio = 0.8 # Nearest neighbor matching ratio
for m, n in nn_matches:
    if m.distance < nn_match_ratio * n.distance:
        matched1.append(kpts1[m.queryIdx])
        matched2.append(kpts2[m.trainIdx])
## [ratio test filtering]

## [homography check]
inliers1 = []
inliers2 = []
good_matches = []
inlier_threshold = 2.5 # Distance threshold to identify inliers with homography check
for i, m in enumerate(matched1):
    col = np.ones((3,1), dtype=np.float64)
    col[0:2,0] = m.pt

    col = np.dot(homography, col)
    col /= col[2,0]
    dist = sqrt(pow(col[0,0] - matched2[i].pt[0], 2) +\
                pow(col[1,0] - matched2[i].pt[1], 2))

    if dist < inlier_threshold:
        good_matches.append(cv.DMatch(len(inliers1), len(inliers2), 0))
        inliers1.append(matched1[i])
        inliers2.append(matched2[i])
## [homography check]

## [draw final matches]
res = np.empty((max(img1.shape[0], img2.shape[0]), img1.shape[1]+img2.shape[1], 3), dtype=np.uint8)
cv.drawMatches(img1, inliers1, img2, inliers2, good_matches, res)
cv.imwrite("matching_result.png", res)

inlier_ratio = len(inliers1) / float(len(matched1))
print(f'{args.feature} Matching Results')
print('*******************************')
print('# Keypoints 1:                        \t', len(kpts1))
print('# Keypoints 2:                        \t', len(kpts2))
print('# Matches:                            \t', len(matched1))
print('# Inliers:                            \t', len(inliers1))
print('# Inliers Ratio:                      \t', inlier_ratio)

cv.namedWindow(window)
cv.imshow(window, res)
cv.waitKey()
## [draw final matches]
