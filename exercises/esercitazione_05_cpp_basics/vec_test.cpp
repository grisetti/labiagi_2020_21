#include <iostream>
#include <iomanip>
#include "matrix.h"
#include "vec.h"
using namespace labiagi;

using Vec3f       = Vec_<float, 3>;
using Vec5i       = Vec_<int, 3>;
  
using namespace std;
int main(){
  Vec3f v1;
  cerr << "zero: " << endl;
  v1=Zero_<Vec3f>;
  cerr << v1 << endl;

  cerr << "access: " << endl;
  v1[0]=0.1;
  v1[1]=0.2;
  v1[2]=0.3;
  cerr << v1 << endl;

  cerr << "squaredNorm: " << endl;
  cerr << v1.squaredNorm() << endl;
  
  cerr << "norm: " << endl;
  cerr << v1.norm() << endl;

  cerr << "copy: " << endl;
  auto v2=v1;
  cerr << v2 << endl;

  cerr << "Assign 2" << endl;
  v2[0]=3;
  v2[1]=2;
  v2[2]=1;
  cerr << v2 << endl;

  cerr << "v2 * 0.5f =" << endl;
  cerr << v2*0.5f << endl;

  cerr << "v2 *= 0.5f =" << endl;
  v2*=0.5f;
  cerr << v2 << endl;

  cerr << v1 << "+" << v2 << "=" << endl;
  cerr <<  v1+v2 << endl;

  cerr << v1 << "-" << v2 <<  "=" << endl;
  cerr <<  v1-v2 << endl;
}
