#pragma once
#include <cassert>
#include <vector>
#include <cmath>
#include "common.h"

namespace labiagi {

  using namespace std;

  // fixed size vector class for quick operations
  template< typename Scalar_, int Dim_>
  class Vec_ {
  public:
    using Scalar = Scalar_;
    using ThisType = Vec_<Scalar_, Dim_>;
    static constexpr int Dim = Dim_;
    inline Scalar& operator[](int i) {return values[i];}
    inline const Scalar& operator[](int i) const {return values[i];}

    // static function that returns a vector with all zeros
    static const Vec_<Scalar, Dim>& Zero() {
      static Vec_<Scalar, Dim> v0;
      v0.setZero();
      return v0;
    }
    
    // sets this to 0
    inline void setZero() {
       for (int i=0; i<Dim; ++i)
         values[i]*=Zero_<Scalar>;
    }

    // multiplies the object by a scalar
    inline ThisType& operator *= (const Scalar s) {
      for (int i=0; i<Dim; ++i)
        values[i]*=s;
      return *this;
    }

    // returns the result of vector multiplied by a scalar
    inline ThisType operator * (const Scalar s) {
      ThisType v(*this);
      v*=s;
      return v;
    }

    // sums to the calling object another vector
    inline ThisType& operator += (const ThisType& other) {
      for (int i=0; i<Dim; ++i)
        values[i]+=other.values[i];
      return *this;
    }

    // subtracts from the calling object another vector
    inline ThisType& operator -= (const ThisType& other) {
      for (int i=0; i<Dim; ++i)
        values[i]-=other.values[i];
      return *this;
    }

    // vector sum
    inline ThisType operator + (const ThisType& other) {
      ThisType returned(*this);
      returned+=other;
      return returned;
    }

    // vector difference
    inline ThisType operator - (const ThisType& other) {
      ThisType returned(*this);
      returned-=other;
      return returned;
    }

    // dot product
    inline Scalar dot(const ThisType& other) const {
      Scalar acc=Zero_<Scalar>;
      for (int i=0; i<Dim; ++i)
        acc+=values[i]*other.values[i];
      return acc;
    }

    // dot product of a vector with self
    inline Scalar squaredNorm() const {
      return dot(*this);
    }

    // vector norm
    inline Scalar norm() const {
      return ::sqrt(squaredNorm());
    }
  protected:
    Scalar values [Dim];
  };
    

  // this complicated below allows to print a generic Vec_<S,V> on a stream
  // the
  //    template < A1, B1, template <A,B> class C_ > is a matching rule for class
  // C<A1, B1> that matches any instance of template class C.
  template < typename Scalar_, int Dim_, template <typename S, int D> class Vec_ >
  ostream & operator << (ostream& os, const Vec_<Scalar_, Dim_>& v){
    os << "(";
    for (int i=0; i<v.Dim; ++i){
      os << v[i];
      if (i<v.Dim-1)
         os << ", ";
    }
    os << ")";
    return os;
  }

  // define the global zero constant
  // matching template rule as above
  template < typename Scalar_, int Dim_,
             template <typename  S, int D> class Vec_ >
  static const Vec_<Scalar_, Dim_> Zero_< Vec_<Scalar_, Dim_> > =Vec_<Scalar_, Dim_>::Zero();

  
}
