#include "image.h"

namespace labiagi {
  using namespace std;
  void ImageMono::applyHomographyTo(ImageMono& dest, const Mat3f& hom) const {
    std::fill(dest.begin(), dest.end(), Zero_<unsigned char>);
    for (int r=0; r<rows(); ++r) {
      for (int c=0; c<cols(); ++c) {
        Vec3f this_coords;
        this_coords[0]=c;
        this_coords[1]=r;
        this_coords[2]=1;

        Vec3f dest_coords=hom*this_coords;
        // normalize
        float z=dest_coords[2];
        dest_coords*=(1./z);

        int dest_r=dest_coords[1];
        int dest_c=dest_coords[0];

        if (dest_r>=0
            && dest_r<dest.rows()
            && dest_c>=0
            && dest_c<dest.cols())
          dest.at(dest_r, dest_c)=at(r,c);
      }
    }
  }

  void ImageMono::applyHomographyFrom(const ImageMono& src,
                                      const Mat3f& inverse_hom,
                                      bool interpolate) {
    std::fill(begin(), end(), Zero_<unsigned char>);
    for (int r=0; r<rows(); ++r) {
      for (int c=0; c<cols(); ++c) {
        Vec3f this_coords;
        this_coords[0]=c;
        this_coords[1]=r;
        this_coords[2]=1;

        Vec3f src_coords=inverse_hom*this_coords;
        //cerr << this_coords <<  "->" << src_coords << endl;
        float z=src_coords[2];
        src_coords*=(1./z);

        if (interpolate) {
          at(r, c)=src.bilinear(src_coords[1], src_coords[0]);
        } else {
          int src_r=src_coords[1];
          int src_c=src_coords[0];

          if (src_r>=0
              && src_r<src.rows()
              && src_c>=0
              && src_c<src.cols())
            at(r,c) = src.at(src_r, src_c);
        }
      }
    }
  }

  istream& operator >> (istream& is, ImageMono& img) {
    std::string tag;
    is >> tag;
    if (tag != "P5") {
      throw runtime_error("invalid pgm");
    }
    int cols, rows;
    is >> cols >> rows;
    int maxval;
    is >> maxval;
    if (maxval >255) {
      throw runtime_error("invalid depth < 8 bit not supported");
    }
    cerr << "maxval: " << maxval << endl;
    img.resize(rows, cols);
    is.read((char*)&img[0], img.rows()*img.cols());
    return is;
  }

  ostream& operator << (ostream& os, const ImageMono& img){
    os << "P5" << endl;
    os << img.cols() << " " << img.rows()  <<  " " << 255 << endl;
    os.write((const char*)&img[0], img.size());
    return os;
  }

}
