#pragma once
#include "vec.h"
namespace labiagi {

  template <typename Scalar_, int Rows_, int Cols_>
  class Mat_ {
  public:
    using Scalar = Scalar_;
    static constexpr int Rows= Rows_;
    static constexpr int Cols= Cols_;
    using RowVectorType=Vec_<Scalar, Cols>;
    using ColVectorType=Vec_<Scalar, Rows>;
    
    // row access m[r] returns the row vector
    inline RowVectorType& operator[](int r) {return _rows[r];}

    // row access m[r] returns the row vector, const version
    inline const RowVectorType& operator[](int r) const {return _rows[r];}
  
    // product Mat_ * Vector
    inline ColVectorType operator*(const RowVectorType& v) const {
      ColVectorType dest;
      for (int r=0; r<Rows; ++r)
        dest[r]=_rows[r].dot(v);
      return dest;
    }

    // sets a matrix to zero
    inline void setZero() {
      for (int r=0; r<Rows; ++r)
        _rows[r].setZero();
    }
    
    // const expression, returning a zero matrix
    static const Mat_<Scalar, Rows, Cols>& Zero() {
      static Mat_<Scalar, Rows, Cols> m0;
      m0.setZero();
      return m0;
    }


    // computes a transposed.
    // it returns a new type!!!
    Mat_<Scalar_, Cols, Rows> transpose() const {
      Mat_<Scalar_, Cols, Rows> returned;
      for (int r=0; r<Rows; ++r)
        for (int c=0; c<Cols; ++c)
          returned[c][r] = _rows[r][c];
      return returned;
    }

    // computes A*B, it returns a new type
    template <int OtherCols_>
    Mat_<Scalar_, Cols_, OtherCols_> operator * (const Mat_<Scalar_, Cols_, OtherCols_>& other) {
      Mat_<Scalar_, Cols_, OtherCols_> returned;
      returned.setZero();
      for (int r=0; r<returned.Rows; ++r)
        for (int c=0; c<returned.Cols; ++c)
          for (int k=0; k<Rows; ++k)
            returned[r][c] += _rows[r][k]*other[k][c];
      return returned;
    }

  
  protected:
    // array of row vectors [ fixed size]
    RowVectorType _rows[Rows_];
  };

  // this one is to do cerr << my_matrix
  template < typename Scalar_, int Rows_, int Cols_,
             template <typename  S, int R, int C> class Matrix_ >
  ostream & operator << (ostream& os, const Matrix_<Scalar_, Rows_, Cols_>& m) {
    for (int r=0; r<m.Rows; ++r) {
      os << m[r] << endl;
    }
    return os;
  }

  // define the global zero constant
  // matching template rule for Matrix_<anytype, anurows, anycols>
  template < typename Scalar_, int Rows_, int Cols_,
             template <typename  S, int R, int C> class Matrix_ >
  static const Matrix_<Scalar_, Rows_, Cols_>
  Zero_< Matrix_<Scalar_, Rows_, Cols_> > =Matrix_<Scalar_, Rows_, Cols_>::Zero();


  
}
