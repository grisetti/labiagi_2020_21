#include "image.h"

namespace labiagi {
  using namespace std;

  // applies an isometry
  // for each pixel in this, it computes the destination pixel in dest
  // and copies the value

  // exercise 1
  void ImageMono::applyHomographyTo(ImageMono& dest, const Mat3f& hom) const {
    std::fill(dest.begin(), dest.end(), 0);
    for (int r=0; r<rows(); ++r) {
      for (int c=0; c<cols(); ++c) {
        Vec3f this_coords;
        this_coords[0]=c;
        this_coords[1]=r;
        this_coords[2]=1;

        // apply the isometry to the the point Mat_ and Vec_ support the multiplication 
        Vec3f dest_coords;
        //dest_coords= ????
        float z=dest_coords[2];

        // remember to multiply dest_coords by 1/z. Vec3 supports *=;
        // dest_coords*= ?

        // calculate the target pixel in integer coordinates
        int dest_r=dest_coords[1];
        int dest_c=dest_coords[0];

        // check if inside image
        if (dest_r>=0
            /*&& ... */) {
          // use the at(r,c) method to access a pixel
          dest.at(dest_r, dest_c)=at(r,c);
        }
      }
    }
  }

  // exercise 2. we copy from src to this
  
  void ImageMono::applyHomographyFrom(const ImageMono& src,
                                      const Mat3f& inverse_hom,
                                      bool interpolate) {
    std::fill(begin(), end(), Zero_<unsigned char>);
    for (int r=0; r<rows(); ++r) {
      for (int c=0; c<cols(); ++c) {
        Vec3f this_coords;
        this_coords[0]=c;
        this_coords[1]=r;
        this_coords[2]=1;

        // for each pixel we see where it comes from

        Vec3f src_coords;
        //src_coords=??;
        
        //cerr << this_coords <<  "->" << src_coords << endl;
        float z=src_coords[2];
        src_coords*=(1./z);

      
        if (interpolate) {
          // exercise 3 use the bilinear(row, col) method to get an interpolated pixel value
          /*at(r, c)= ??*/
        } else {
          //exercise 2: retrieve the pixel coordinates
          int src_r=src_coords[1];
          int src_c=src_coords[0];

          if (src_r>=0
              && src_r<src.rows()
              && src_c>=0
              && src_c<src.cols())
            at(r,c) = src.at(src_r, src_c);
        }
      }
    }
  }

  istream& operator >> (istream& is, ImageMono& img) {
    std::string tag;
    is >> tag;
    if (tag != "P5") {
      throw runtime_error("invalid pgm");
    }
    int cols, rows;
    is >> cols >> rows;
    int maxval;
    is >> maxval;
    if (maxval >255) {
      throw runtime_error("invalid depth < 8 bit not supported");
    }
    cerr << "maxval: " << maxval << endl;
    img.resize(rows, cols);
    is.read((char*)&img[0], img.rows()*img.cols());
    return is;
  }

  ostream& operator << (ostream& os, const ImageMono& img){
    os << "P5" << endl;
    os << img.cols() << " " << img.rows()  <<  " " << 255 << endl;
    os.write((const char*)&img[0], img.size());
    return os;
  }

}
