#pragma once
#include <cassert>
#include <vector>
#include <cmath>
#include "defs.h"

namespace labiagi {

  using namespace std;

  // variable size matrix class The item type is BaseType_'
  // it relies on a std::vector as storage.
  // Allocator can be used defined (use the default one)
  
  template <typename BaseType_, typename AllocatorType_=std::allocator<BaseType_> >
  class Matrix_: public std::vector<BaseType_, AllocatorType_> {
  public:
    using value_type     = BaseType_;
    using Scalar         = value_type;
    using allocator_type = AllocatorType_;
    using VectorType     = std::vector<BaseType_, AllocatorType_>;
    using ThisType       = Matrix_<Scalar, AllocatorType_>;
    // empty ctor
    Matrix_(){}

    // resizing ctor
    Matrix_(int rows_, int cols_){
      resize(rows_, cols_);
    }
    
    // num of rows
    inline int rows() const {return _rows;}

    // num of cols
    inline  int cols() const {return _cols;}

    // const element access
    inline const value_type& at(int r, int c) const{
      assert(r<rows() && c<cols() && __PRETTY_FUNCTION__);
      return VectorType::at(r*cols()+c);
    }
    
    // element access
    inline value_type& at(int r, int c) {
      return VectorType::at(r*cols()+c);
    }


    // bilinear interpolation on the value
    // returns a the value at a float position
    // by "averaging" the values in the neighborhood
    inline value_type bilinear(float r, float c) const {
      if (r<0||r > (rows()-2) )
        return Zero_<value_type>;

      if (c<0||c > (cols()-2) )
        return Zero_<value_type>;
      const int r0=::floor(r);
      const int c0=::floor(c);
      float dr=r-r0;
      float dc=c-c0;
      const value_type v00=at(r0,c0);
      const value_type v01=at(r0,c0+1);
      const value_type v10=at(r0+1, c0);
      const value_type v11=at(r0+1, c0+1);
      return
        (1.f-dr)*v00*(1.f-dc)
        + (1-dr)*v01*(dc)
        + (dr)*v10*(1.f-dc)
        + (dr)*v11*(dc);
    }

    // same as above
    inline value_type bilinear(const Vec2f& pos) const {
      return bilinear(pos[0], pos[1]);
    }
    
    // resizes a matrix.
    // the elements are shuffled!!!!
    void resize(int r, int c) {
      VectorType::resize(r*c);
      _rows=r;
      _cols=c;
    }

    template <int KernelRows_, int KernelCols_>
    inline Scalar dot(int r_start, int c_start, const Mat_<Scalar, KernelRows_, KernelCols_>& kernel) const {
      Scalar acc=Scalar(0);
      for (int r=0; r<KernelRows_; ++r) {
        const Scalar* row_ptr=&at(r_start+r,c_start);
        for (int c=0; c<KernelCols_; ++c) {
          acc+= row_ptr[c]*kernel[r][c];
        }
      }
      return acc;
    }

    template <int KernelRows_, int KernelCols_>
    inline Scalar convolveTo(ThisType& dest, const Mat_<Scalar, KernelRows_, KernelCols_>& kernel) const {
      constexpr int KernelRowsMiddle=(KernelRows_-1)/2;
      constexpr int KernelColsMiddle=(KernelCols_-1)/2;
      dest.resize(rows, cols);
      std::fill(dest.begin(), dest.end(), Zero_<Scalar>);
      for (int r=KernelRowsMiddle; r<rows()-KernelRowsMiddle; ++r)
        for (int c=KernelColsMiddle; c<cols()-KernelColsMiddle; ++c)
          dest.at(r,c)=dot(r-KernelRowsMiddle,c-KernelColsMiddle, kernel);
    }
    
  protected:
    int _rows=0, _cols=0;
  };
  

  // operator << for printing
  // matching template rule for Matrix_<anytype>
  template < typename Scalar_, template <typename S> class Matrix_ >
  ostream & operator << (ostream& os, const Matrix_<Scalar_>& m){
    os << "matrix:  " << &m
       << " rows: " << m.rows()
       << " cols: " << m.cols()
       << endl;
    for (int r=0; r<m.rows(); ++r){
      for (int c=0; c<m.cols(); ++c){
        os << m.at(r,c) << " ";
      }
      os << endl;
    }
    return os;
  }
}
