#include <iostream>
#include <fstream>
#include "matrix.h"
#include "vec.h"
#include "image.h"

using namespace labiagi;
using namespace std;
int main(int argc, char** argv){
  if (argc<6) {
    cerr << " usage: " << endl;
    cerr << " tests the applyHomographyTo function " << endl;
    cerr << argv[0] << " <src_image> <x> <y> <theta> <dest_image>" << endl;
    return -1;
  }
  ifstream is(argv[1]);
  if (! is.good()) {
    cerr << " cant read file: " << argv[1] << endl;
  }
    
  ImageMono img;
  is >> img;
  cerr << "loaded image, width: " << img.cols() << " height: " << img.rows() << endl;

  // read parameters from the command line
  float tx = atof(argv[2]);
  float ty = atof(argv[3]);
  float theta = atof(argv[4]);

  Mat3f iso=Mat3f::Zero();
  // 1. assemble isometry from tx, ty and tz and put the elements in a matrix.
  // the matrix elements are accessed with m[row][col]
  float s=sin(theta), c=cos(theta);
  /*
  iso[0][0]=...
  iso[0][1]=...

  */
  iso[2][2]=1;

  cerr << "isometry: " << endl << iso << endl;
  
  ImageMono img2=img;
  // implement the function applyHomography (see image.cpp),
  img.applyHomographyTo(img2, iso);

  ofstream os(argv[5]);
  os << img2;
  cerr << "written image, width: " << img2.cols() << " height: " << img2.rows() << endl;
  
}
