#pragma once
namespace labiagi {
  
  // zero function, returns the null element of T
  // use partial specialization to adapt it to your own type
  template <typename T>
  static const T Zero_=T(0);

  

}
