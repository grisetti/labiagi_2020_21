#include <iostream>
#include <fstream>
#include "matrix.h"
#include "vec.h"
#include "image.h"

using namespace labiagi;
using namespace std;
int main(int argc, char** argv){
  if (argc<6) {
    cerr << " usage: " << endl;
    cerr << argv[0] << " <src_image> <x> <y> <theta> <dest_image>" << endl;
    return -1;
  }
  ifstream is(argv[1]);
  if (! is.good()) {
    cerr << " cant read file: " << argv[1] << endl;
  }
    
  ImageMono img;
  is >> img;
  cerr << "loaded image, width: " << img.cols() << " height: " << img.rows() << endl;

  // read parameters from the command line
  float tx = atof(argv[2]);
  float ty = atof(argv[3]);
  float theta = atof(argv[4]);

  // assemble isometry
  float s=sin(theta), c=cos(theta);
  Mat3f iso=Mat3f::Zero();
  iso[0][0]=c; iso[0][1]=-s; iso[0][2]=tx;
  iso[1][0]=s; iso[1][1]=c;  iso[1][2]=ty;
  iso[2][2]=1;
  //
  cerr << "Isometry: " << endl << iso << endl;

  Mat3f inv_iso=iso;
  inv_iso[0][0]=c;  inv_iso[0][1]=s;  inv_iso[0][2]=- ( c  * tx + s * ty );
  inv_iso[1][0]=-s; inv_iso[1][1]=c; inv_iso[1][2]=- ( -s * tx + c * ty );
  cerr << "Inverse Isometry: " <<  endl << inv_iso << endl;

  
  ImageMono img2=img;
  //img.applyHomographyTo(img2, iso);
  
  img2.applyHomographyFrom(img, inv_iso, true);

  ofstream os(argv[5]);
  os << img2;
  cerr << "written image, width: " << img2.cols() << " height: " << img2.rows() << endl;

  
}
