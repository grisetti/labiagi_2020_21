#pragma once
#include <iostream>
#include "matrix.h"

namespace labiagi {
  
  class ImageMono: public Matrix_<unsigned char>{
    // Rows: number of rows;
    // Cols: number of cols
    // at(r,c) returns a pixel reference (const/ non const);
  public:
    // implement the one below for exercise 1
    void applyHomographyTo(ImageMono& dest, const Mat3f& hom) const;

    // implement the one below for
    //   interpolate=false for exercise 2,
    // and
    //   interpolate=true for exercise 3,
    // implementation in image.cpp
    void applyHomographyFrom(const ImageMono& src, const Mat3f& inverse_hom, bool interpolate=false);
  };


  // reads an image from a png file
  std::istream& operator >> (std::istream& is, ImageMono& img);

  // writes an image to a png file
  std::ostream&  operator << (std::ostream& os, const ImageMono& img);

}

  
