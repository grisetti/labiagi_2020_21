#pragma once
#include "vec.h"
#include "mat.h"
namespace labiagi {
  
  using Vec2i=Vec_<int, 2>;
  using Vec2f=Vec_<float, 2>;
  using Vec3i=Vec_<int, 3>;
  using Vec3f=Vec_<float, 3>;
  using Mat3f=Mat_<float, 3, 3>;

}
