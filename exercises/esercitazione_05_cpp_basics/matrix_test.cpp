#include <iostream>
#include <iomanip>
#include "matrix.h"
#include "vec.h"
using namespace labiagi;

using Vec3f       = Vec_<float, 3>;
using MatrixFloat = Matrix_<float>;
using MatrixChar  = Matrix_<char>;
using MatrixVec3f = Matrix_<Vec3f>;

using namespace std;
int main(){
  MatrixFloat m1(10, 10);
  // populate the matrix
  for (int r=0; r<m1.rows(); ++r)
    for (int c=0; c<m1.cols(); ++c)
      m1.at(r,c)=sqrt((1+r)*(2+c));

  // set the stream as fixed, with precision 3 digits
  cerr << std::fixed << std::setprecision(3) << endl;
  cerr << m1 << endl;

  cerr <<  "copy: "<< endl;
  MatrixFloat m2=m1;
  cerr << m2 << endl;

  cerr << "resize: " << endl;
  m2.resize(5,5);
  cerr << m2 << endl;
  
  // try interpolate
  cerr <<  "value at 0, 0:     " << m1.bilinear(0.0f, 0.0f) << endl;
  cerr <<  "value at 0.9, 0:   " << m1.bilinear(0.9f, 0.0f) << endl;
  cerr <<  "value at 0, 0.9:   " << m1.bilinear(0.0f, 0.9f) << endl;
  cerr <<  "value at 0.9, 0.9: " << m1.bilinear(0.9f, 0.9f) << endl;

  cerr << "clear: " << endl;
  std::fill(m2.begin(), m2.end(), 0);
  cerr << m2;

  MatrixVec3f mv(5,5);
  std::fill(mv.begin(), mv.end(), Zero_<Vec3f>);
  cerr << mv << endl;
}
