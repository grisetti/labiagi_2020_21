#include <iostream>
#include <iomanip>
#include "mat.h"
using namespace labiagi;

using Mat3f       = Mat_<float, 3, 3>;
using Mat3_5f     = Mat_<float, 3, 5>;
using Mat4_6f     = Mat_<float, 4, 6>;
using Vec3f       = Vec_<float, 3>;

using namespace std;
int main(){
  Mat3f m;
  float theta=0.5;
  float x=0;
  float y=0;
  float s=sin(theta);
  float c=cos(theta);
  m[0][0] = c ;   m[0][1] = -s  ; m[0][2] = x;
  m[1][0] = s ;   m[1][1] =  c  ; m[1][2] = y; 
  m[2][0] = 0 ;   m[2][1] =  0  ; m[2][2] = 1; 

  cerr << "Isometry 2D" << endl;
  cerr << m << endl;

  Vec3f v;
  v[0] = 0.1;
  v[1] = 2;  
  v[2] = 1.f; // make it homo

  cerr << "mult a homo vec by an iso" << endl;
  auto v_out= m*v;

  Mat3_5f m_other;
  for (int r=0; r<m_other.Rows; ++r)
    for (int c=0; c<m_other.Cols; ++c)
      m_other[r][c] = (r==2) ? 1.f : (r+1)*(c+2);

  cerr << "assemble a flock of points, by vector" << endl;
  cerr << m_other << endl;

  cerr << "trappose 'em" << endl;
  cerr << m_other.transpose() << endl;

  cerr << "traffom 'em" << endl;
  auto m_mult=m*m_other;
  cerr << m_mult << endl;
  

  Mat3f minv;
  minv[0][0] =  c ;   minv[0][1] = s   ; minv[0][2] = -c*x-s*y;
  minv[1][0] = -s ;   minv[1][1] =  c  ; minv[1][2] =  s*x-c*y; 
  minv[2][0] =  0 ;   minv[2][1] =  0  ; minv[2][2] = 1; 

  cerr << "untraffom 'em, should be the same yooo" << endl;
  auto m_de_prima=minv*m_mult;
  cerr << m_de_prima << endl;

  
}
