#include <ros/ros.h>
#include <turtlesim/Pose.h>
#include <turtlesim/TeleportAbsolute.h>
#include <turtlesim/SpawnCircle.h>
#include <turtlesim/SetPen.h>
#include <geometry_msgs/Twist.h>
#include <std_srvs/Empty.h>
#include <cmath>

using namespace std;

uint8_t id;
float radius;
float x,y;
float linear_speed=0.5;
float rotation_speed=0;

enum Status {Idle, Teleport, Drawing};

struct LilCircle {
  LilCircle(float x_=0, float y_=0, float r_=0):
    x(x_), y(y_), r(r_){}
  float x,y,r;
};

std::map<int, LilCircle> circles;

Status status=Idle;
uint8_t r=0, g=0, b=0;

ros::Publisher twist_pub;
ros::ServiceClient teleport_srv;
ros::ServiceClient set_pen_srv;

bool spawnCircle(turtlesim::SpawnCircle::Request& req, turtlesim::SpawnCircle::Response& resp) {
  if (status!=Idle)
    return false;
  // draw circle
  if (req.action!=0) {
    // we already have the circle
    if (circles.count(req.circle.id))
      return false;
    x=req.circle.x;
    y=req.circle.y;
    radius=req.circle.radius;
    r=255;
    g=0;
    b=0;
    circles[req.circle.id]=LilCircle(x,y,radius);
    cerr << "drawing circle, id: " << (int) req.circle.id << endl;
    status=Teleport;
  } else {
    auto it=circles.find(req.circle.id);
    if (it==circles.end()) {
      return false;
    }
    cerr << "erasing circle, id: " << (int) req.circle.id << endl;
    x=it->second.x;
    y=it->second.y;
    radius=it->second.r;
    r=0;
    g=0;
    b=255;
    status=Teleport;
    circles.erase(it);
  }
  resp.circles.resize(circles.size());
  int k=0;
  for (auto it: circles){
    turtlesim::Circle& c=resp.circles[k];
    c.id=it.first;
    c.x=it.second.x;
    c.y=it.second.y;
    c.radius=it.second.r;
  }
  return true;
}

void currentPoseCallback(const turtlesim::PoseConstPtr& pose) {
  switch (status) {
  case Idle:
    break;
  case Teleport: {
    turtlesim::SetPen pen;
    pen.request.off=true;
    set_pen_srv.call(pen);
    turtlesim::TeleportAbsolute msg;
    msg.request.x=x+radius;
    msg.request.y=y;
    msg.request.theta=M_PI/2;
    teleport_srv.call(msg);
    pen.request.off=false;
    pen.request.r=r;
    pen.request.g=g;
    pen.request.b=b;
    set_pen_srv.call(pen);
    rotation_speed=linear_speed/radius;
    status=Drawing;
  }
  break;
  case Drawing: {
    float dth=pose->theta-M_PI/2;
    dth=atan2(sin(dth),cos(dth));
    if (dth<0 && dth>-M_PI/20) {
      status=Idle;
      turtlesim::SetPen pen;
      pen.request.off=true;
      set_pen_srv.call(pen);
    } else {
      geometry_msgs::Twist msg;
      msg.linear.x=linear_speed;
      msg.linear.y=0;
      msg.linear.z=0;
      msg.angular.x=0;
      msg.angular.y=0;
      msg.angular.z=rotation_speed;
      twist_pub.publish(msg);
    }
  }
    break;
  default:;
  }
}

void commandTurtle(ros::Publisher twist_pub, float linear, float angular)
{
  
  geometry_msgs::Twist twist;
  twist.linear.x = linear;
  twist.angular.z = angular;
  twist_pub.publish(twist);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "draw_square");
  ros::NodeHandle nh;
  ros::Subscriber pose_sub = nh.subscribe("turtle1/pose", 1, currentPoseCallback);
  twist_pub = nh.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1);
  teleport_srv = nh.serviceClient<turtlesim::TeleportAbsolute>("turtle1/teleport_absolute");
  set_pen_srv = nh.serviceClient<turtlesim::SetPen>("turtle1/set_pen");
  ros::ServiceServer service = nh.advertiseService("turtle1/spawn_circle", spawnCircle);
  ros::spin();
}
